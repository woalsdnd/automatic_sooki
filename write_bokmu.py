# 출근기록부의 지각내역 및 docs wave 의 출장내역을 복무기록부에 자동으로 기재하는 script
#
# 사용 방법
# 1. write_bokmu.py 와 같은 폴더에 "choolgun" 이라는 폴더를 만든다
# 2. "choolgun" 폴더 밑에 출근기록부의 이름을 {yyyy}_{mm}.pdf 형식으로 변경한뒤 놓아둔다 (ex - 2019_09.pdf)
# 3. "choolgun" 폴더 밑에 {본인이름}_누적.txt (ex - 손재민_누적.txt) 파일을 만든뒤 지단달까지의 누계를 "x일 x분" 포맷으로 x와 일, x와 분사이에 띄어씌가 없이 적는다 (누적 파일은 업데이트되고, 연차등은 수기로 수정해야함)
# 4. write_bokmu.py 에서 "PERSONAL INFO" 를 문자열 검색한뒤 이름, 편입일자, 생년월일을 적는다  
# 5. write_bokmu.py 를 실행한다 (command: python3 write_bokmu.py)
# 6. "choolgun" 폴더 밑에 {yyyy}_{mm}_{index}.docx 가 생성되었는지 확인한다  
# 7. {yyyy}_{mm}_{index}.docx 를 켠후 레이아웃 탭 -> 크기 아이콘을 클릭하여 A4 로 변경
#
# 구현시 가정
# - txt 로 변환시 근무스케줄은 순서가 뒤바뀌지 않는다고 가정 (debugging 한 결과 지금까지는 순서 바뀌는 경우 없었음) 
# - 15시0분 ~ 23시59분 사이에 찍힌 지문은 퇴근시각으로 처리된다고 가정 
#
# POLICY
# - 출근시각 혹은 퇴근시각이 둘다 안찍힌경우: 연차 or 출장 or 공휴일
# - 출근시각 혹은 퇴근시각이 하나만 안찍힌경우: 반차 or 출장
# - 단, 출근시각, 퇴근시각이 둘다 찍혀있어도 반차일수있음
# - 다음날 새벽에 퇴근하는 경우 (오전 6시 이전에 찍힌 지문) 는 밤 11시 59분 퇴근으로 간주  
#
# 구현 관련 ISSUE
# - 출근기록부를 pdf 에서 txt 변환하면 내용은 보존되지만 값이 출현하는 순서가 뒤바뀌는 경우가 있어 page 단위로 txt 로 변환 
# - 개별사람에 대해서 모든 정보를 다 읽어들인뒤 정보를 분류하여 처리
# - 필수출근일은 토,일을 제외한 날
#
# 구현 관련 NOTE
# - 휴일에 출근한 경우 근무스케줄이 "휴일"로 기재됨
# - 편입이 안된경우 근무스케줄이 "평일"로 기재됨 
# - 근무스케쥴은 "출근:x시x분" 의 문자열
# - 출근시간은 "year-month-date hour:minute" 혹은 "year-month-date hour:minute:second" 의 문자열

import os
import io

import numpy as np
import re
import shutil

from datetime import datetime, timedelta
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter, resolve1
from pdfminer.converter import HTMLConverter, TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument

from docx import Document
from docx.shared import Inches, Pt

#######################
#####PERSONAL INFO#####
#######################
NAME = "손재민"
ENROLL_DATE = "2018년 3월 9일"
BIRTHDATE = "1990년 2월 1일"
LIST_REASON = ["늦잠", "교통체증"]  # 4글자 이내여야 개행안됨
#######################
#######################
 
#############################
###### helper functions #####
#############################


def convert_pdf2txt(pdf_fname, save_dir):

    # get total page number
    with open(pdf_fname, 'rb') as file:
        parser = PDFParser(file)
        document = PDFDocument(parser)
        n_pages = resolve1(document.catalog['Pages'])['Count']
    
    # extract contents and save to a file
    manager = PDFResourceManager() 
    codec = 'utf-8'
    caching = True
    with io.StringIO() as output:
        with TextConverter(manager, output, codec=codec, laparams=LAParams()) as converter:
            interpreter = PDFPageInterpreter(manager, converter)   
            with open(pdf_fname, 'rb') as infile:
                for p_index in range(n_pages):
                    for page in PDFPage.get_pages(infile, [p_index], caching=caching, check_extractable=True):
                        interpreter.process_page(page)
                        convertedPDF = output.getvalue()
                        output.truncate(0)
                        with open(os.path.join(save_dir, "{}.txt".format(p_index)), "w") as f:
                            f.write(convertedPDF)


def parse_time_stamp(time_stamp):
    choolgun_time_split = time_stamp.split(":")
    if len(choolgun_time_split) == 2:
        hour, minute = choolgun_time_split
    elif len(choolgun_time_split) == 3:
        hour, minute, _ = choolgun_time_split
    else:
        print(choolgun_time_split)
        raise ValueError("출퇴근시간 포맷 => year-month-date hour:minute or year-month-date hour:minute:second")
    return hour, minute


#############################
##### misc params, paths ####
#############################
CHOOLGUN_DIR = "choolgun"
SCHEDULE_COL = "근무스케줄"
NAME_COL = "이름"
target_year = datetime.now().strftime('%Y')
target_month = datetime.now().strftime('%m')
target_fn = "{}_{}".format(target_year, str(int(target_month) - 1).zfill(2))
pdf_path = os.path.join(CHOOLGUN_DIR, target_fn + ".pdf")
txt_dir_path = os.path.join(CHOOLGUN_DIR, target_fn)
hisotry_path = os.path.join(CHOOLGUN_DIR, "{}_누적.txt".format(NAME))
with open(hisotry_path, "r") as f:
    lines = f.read().splitlines()
TOTAL_DATE, TOTAL_MINUTE = lines[0].split()
TOTAL_DATE = int(TOTAL_DATE[:TOTAL_DATE.find("일")])
TOTAL_MINUTE = int(TOTAL_MINUTE[:TOTAL_MINUTE.find("분")])
    
##############
# 출근기록부 txt 폴더가 존재하지 않는경우 pdf 에서 생성
##############
if not os.path.exists(txt_dir_path):
    os.makedirs(txt_dir_path, exist_ok=True)
    try:
        convert_pdf2txt(pdf_path, txt_dir_path)
    except:
        shutil.rmtree(txt_dir_path)

##############
# 지각내역을 출근기록부 txt 파일에서 찾아냄
##############
# txt 파일에서 정보뽑기
txt_paths = [os.path.join(txt_dir_path, fname) for fname in os.listdir(txt_dir_path) if fname.endswith(".txt")]
dict_extracted_info = {}
for txt_path in txt_paths:
    with open(txt_path, "r") as f:
        lines = f.read().splitlines()
    list_gunmu_time, list_gunmu_date, list_gunmu_schedule = [], [], []
    for line in lines:
        if NAME_COL in line:  # 이름 정보 저장
            name = line.replace("이름 : ", "")
        if re.compile("20[0-9]{2}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}").match(line):
            date, time = line.split()  # 출근 혹은 퇴근 정보 (year-month-date hour:minute or year-month-date hour:minute:second)
            if int(time.split(":")[0]) < 6:  # 6시 이전의 time stamp 는 전날 11:59 으로 변경
                modified_time_stamp = (datetime.strptime(date, '%Y-%m-%d') - timedelta(days=1)).strftime('%Y-%m-%d') + " 11:59"
                list_gunmu_time.append(modified_time_stamp)                    
            else:
                list_gunmu_time.append(line)
        elif re.compile("20[0-9]{2}-[0-9]{2}-[0-9]{2}").match(line) and len(line) == 10:  # 근무일자 (year-month-date 포맷으로 되어있음)
            list_gunmu_date.append(line)
        if "출근:" in line:  # 근무스케쥴
            time_info = line.replace("출근:", "")
            hour = int(time_info[:time_info.index("시")])
            if "분" in line:  # x시 x분
                minute = int(time_info[time_info.index("시") + 1:time_info.index("분")])
                choolgun_time = hour + 1.*minute / 60
            else:  # x시
                choolgun_time = hour
            list_gunmu_schedule.append(choolgun_time)
        if "평일" in line:  # 편입전인경우로, 출근시간을 7시로 가정 (지각으로 처리하지 않기 위함)
            list_gunmu_schedule.append(7)
    dict_extracted_info[name] = {"list_gunmu_schedule":list_gunmu_schedule,
                                 "list_gunmu_date":list_gunmu_date,
                                 "list_gunmu_time":list_gunmu_time}

# 개인별로 출근일 걸러내기 (출근일=휴일이 아닌날) - 근무일자는 출퇴근 기록부의 가장 왼쪽 컬럼을 지칭
list_name = dict_extracted_info.keys()
dict_required_gunmu_date = {}
for name in list_name:
    dict_required_gunmu_date[name] = []
    for gunmu_date in dict_extracted_info[name]["list_gunmu_date"]:
        if datetime.strptime(gunmu_date, '%Y-%m-%d').weekday() < 5:  # monday ~ friday
            dict_required_gunmu_date[name].append(gunmu_date)

# 개인별로 출퇴근 시간 정리
dict_choolgun = {name:{required_gunmu_date:{"timestamp":[]} for required_gunmu_date in dict_required_gunmu_date[name]} for name in list_name}
for name in list_name:
    for lname, l in dict_extracted_info[name].items():
        if lname == "list_gunmu_time":
            sorted(l)
            for time_stamp in l:
                date, time = time_stamp.split()
                if date in dict_required_gunmu_date[name]:
                    dict_choolgun[name][date]["timestamp"].append(time)
        elif lname == "list_gunmu_schedule":
            assert len(l) == len(dict_required_gunmu_date[name])  # 근무스케줄숫자==필요근무숫자
            for index in range(len(l)):
                dict_choolgun[name][dict_required_gunmu_date[name][index]]["schedule"] = l[index]

# 개인별로 지각시간, 부재정보 정리
dict_late_absence = {name:{required_gunmu_date:{} for required_gunmu_date in dict_required_gunmu_date[name]} for name in list_name}
for name, history in dict_choolgun.items():
    for date, timestamps_schedule in history.items():
        time_stamps = timestamps_schedule["timestamp"]
        choolgun_schedule_in_minute = timestamps_schedule["schedule"] * 60
        if len(time_stamps) == 0:
            dict_late_absence[name][date]["late"] = -1
            dict_late_absence[name][date]["absence"] = "allday"
        elif len(time_stamps) == 1:
            if time_stamps[0] < "15:00":
                choolgun_time = time_stamps[0]
                hour, minute = parse_time_stamp(choolgun_time)
                choolgun_time_in_minute = int(hour) * 60 + int(minute)
                dict_late_absence[name][date]["late"] = max(0, choolgun_time_in_minute - choolgun_schedule_in_minute)
                dict_late_absence[name][date]["absence"] = "afternoon"
            else:
                dict_late_absence[name][date]["late"] = -1
                dict_late_absence[name][date]["absence"] = "morning"
        elif len(time_stamps) == 2:
            choolgun_time = min(time_stamps[0], time_stamps[1])
            hour, minute = parse_time_stamp(choolgun_time)
            choolgun_time_in_minute = int(hour) * 60 + int(minute)
            dict_late_absence[name][date]["late"] = max(0, choolgun_time_in_minute - choolgun_schedule_in_minute)
            dict_late_absence[name][date]["absence"] = "present"
        else:
            raise ValueError("more than 2 time stamps exist")

# 개인별로 지각시간, 부재정보 출력  
for name in list_name:
    print(name)
    for date in dict_required_gunmu_date[name]:
        print(date, dict_late_absence[name][date])

# 개인별로 지각일 체크하고 출력 페이지수 결정
list_late_days = [date for date, late_info in dict_late_absence[NAME].items() if late_info['late'] > 0]
n_late_days = len(list_late_days)
if n_late_days == 0:
    n_pages = 0
elif n_late_days <= 11:
    n_pages = 1
else:
    n_pages = 2

# 반차, 결석 의심 날짜 출력
print("======== SUMMARY FOR {} ========".format(NAME))
for date, late_info in dict_late_absence[NAME].items():
    if late_info['late'] < 0:
        print("{} : {} (absent)".format(date, late_info['absence']))

# 도큐먼트 출력
for index_document in range(n_pages):
    # generate document
    document = Document()
    style = document.styles['Normal']
    style.font.size = Pt(8.5)
    document.add_paragraph('■ 전문연구요원 및 산업기능요원의 관리규정 [별지 제6호서식] <개정 ’11.9.29, 2017. 7. 3.>', style='Normal')
    document.add_heading('\t\t\t\t\t개인별 복무상황부\n\t\t\t\t(전문연구요원/산업기능요원)', 1)
    # table header
    table_header = document.add_table(rows=2, cols=3)
    table_header.style = document.styles['Medium Grid 1']
    cells_first_row = table_header.rows[0].cells
    merged_cell = cells_first_row[0].merge(cells_first_row[1])
    merged_cell.text = '소속  (주)VUNO 기업부설연구소'
    cells_first_row[2].text = '편입일자  {}'.format(ENROLL_DATE)
    second_cells = table_header.rows[1].cells
    second_cells[0].text = '이름  {}'.format(NAME)
    second_cells[1].text = '생년월일 {}'.format(BIRTHDATE)
    second_cells[2].text = '확인자\n\t\t(서명 또는 인)'
    # body table
    n_rows_body, n_cols_body = 13, 10
    table_body = document.add_table(rows=n_rows_body, cols=n_cols_body)
    table_body.style = document.styles['Table Grid']
    merged_cell1 = table_body.rows[0].cells[0].merge(table_body.rows[1].cells[0])
    merged_cell1.text = "① 구분 (출장, 외출 등)"
    merged_cell2 = table_body.rows[0].cells[1].merge(table_body.rows[0].cells[2])
    merged_cell2 = merged_cell2.merge(table_body.rows[0].cells[3])
    merged_cell2.text = "② 기간 또는 일시"
    table_body.rows[1].cells[1].text = "부터"
    table_body.rows[1].cells[2].text = "까지"
    table_body.rows[1].cells[3].text = "일수"
    merged_cell3 = table_body.rows[0].cells[4].merge(table_body.rows[0].cells[5])
    merged_cell3.text = "③ 내    용"
    table_body.rows[1].cells[4].text = "사유, 목적"
    table_body.rows[1].cells[5].text = "장  소"
    merged_cell4 = table_body.rows[0].cells[6].merge(table_body.rows[1].cells[6])
    merged_cell4.text = " ④ 누   계 (   일  시간)"
    merged_cell5 = table_body.rows[0].cells[7].merge(table_body.rows[0].cells[8])
    merged_cell5.text = " ⑤ 결    재 "
    table_body.rows[1].cells[7].text = "담당"
    table_body.rows[1].cells[8].text = "업체장"
    merged_cell5 = table_body.rows[0].cells[9].merge(table_body.rows[1].cells[9])
    merged_cell5.text = "⑥ 비 고"
    for row in range(2, n_rows_body):
        table_body.rows[row].cells[6].text = "\n"  # 6th column not modified
    # table footer 
    table_footer = document.add_table(rows=1, cols=1)
    table_footer.style = document.styles['Medium Grid 1']
    table_footer.rows[0].cells[0].text = "\t\t\t\t\t작성요령 및 유의사항"
    document.add_paragraph('① 구분 : 출장, 외출, 지각, 조퇴, 병가, 연가 등 근무상황 기재\n② 기간 또는 일시 : 구분에 기재된 근무상황의 기간 또는 일시 기재(지각․조퇴․외출․결근 등은 시간까지 기재)\n③ 내용 : 구분에 기재된 근무상황의 사유․목적, 장소 기재\n④ 누계 : 구분에 기재된 근무상황을 병가/연가로 구분하여 기간(시간) 누계 기재\n\t(예시 : 병가 3일 5시간, 연가 7일 2시간 등)\n - 병가 : 질병으로 인한 지각·조퇴·외출·결근 등을 기재하며, 편입일부터 만료일까지 누계 작성\n - 연가 : 질병 외 지각·조퇴·외출·결근 등을 기재하며, 업체에서 정한 기준일부터 1년 단위로 작성\n\t(예시 : 7.1.이 기준일인 경우, 7.1.~다음해 6.30.까지 누계 작성)\n (*) 병가 및 연가 누계 작성 시, 지각·외출·조퇴·결근 등으로 누계 8시간 일 경우 1일로 계산하며,\n  개인별 병가 및 연가 초과 시 그 기간만큼 연장 복무\n (*) 연장복무대상은 사유 발생일부터 14일이내에 관할 지방병무청장에게 연장복무일수를 통보', style='Normal')
    
    # 지각 정보를 docx 에 기입 
    index_row = 2
    for index_row in range(2, 13):
        if index_document * 10 + index_row - 2 < len(list_late_days):
            date = list_late_days[index_document * 10 + index_row - 2]
            late_info = dict_late_absence[NAME][date]
            TOTAL_MINUTE += late_info['late']
            cells = table_body.rows[index_row].cells
            cells[0].text = "지각"
            cells[1].text, cells[2].text = date, date
            cells[3].text = "{}분".format(late_info['late'])
            cells[4].text = LIST_REASON[np.random.randint(len(LIST_REASON))]
            cells[6].text = "{}일 {}분".format(TOTAL_DATE, TOTAL_MINUTE)

    # 도큐먼트 출력
    document.save(os.path.join(CHOOLGUN_DIR, '{}_{}.docx'.format(target_fn, index_document)))

# 누적 지각분 업데이트
with open(hisotry_path, "w") as f:
    f.write("{}일 {}분".format(TOTAL_DATE, TOTAL_MINUTE))
